package pipez;

import java.util.Arrays;

import pipez.core.Block;
import pipez.core.Pipe;
import pipez.core.PipezExceptionHandler;
import pipez.core.SimpleBlock;
import static pipez.core.SpecialBlocks.*;

public class SortPipe implements Pipe {
	
	private String toMatch = null;
	private boolean ignoreCase;
	
	private SortPipe(String toMatch, boolean ignoreCase) {
		this.toMatch = toMatch;
		this.ignoreCase = ignoreCase;
	}
	
	public static SortPipe create() {
		return new SortPipe(null, false);
	}
	
	public static SortPipe create(String toMatch) {
		return new SortPipe (toMatch, false);
	}

	public static SortPipe createIgnoreCase(String toMatch) {
		return new SortPipe(toMatch, true);
	}
	
	@Override
	public String getName() {
		return "Sort Data";
	}
	
	@Override
	public Block transform(Block block) {
		
		if (toMatch == null) { //field sorting
			SimpleBlock newBlock = new SimpleBlock();
			String [] fields = block.fields();
			Arrays.sort(fields);
			
			for (int i = 0; i < fields.length; i++) {
				newBlock.add(fields[i], block.value(fields[i]));			
			}
			return newBlock;
		} else {
			//value sorting
			SimpleBlock newBlock = new SimpleBlock();
			String [] fields = block.fields();
			
			for (String v: block.values()) {
				if (ignoreCase) {
					if (v.equalsIgnoreCase(toMatch)) {
						String [] values = block.values();
						Arrays.sort(values);
						
						for (int i = 0; i < values.length; i++) {
							newBlock.add(fields[i], values[i]);			
						}
						return newBlock;
					}
				} else if (!ignoreCase) {
					if (v.equals(toMatch)) {
						String [] values = block.values();
						Arrays.sort(values);
						
						for (int i = 0; i < values.length; i++) {
							newBlock.add(fields[i], values[i]);			
						}
						return newBlock;
					}
				}
			}
		}
		return SKIP_BLOCK;	
	}
}