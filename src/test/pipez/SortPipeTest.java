package pipez;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;
import static pipez.util.TestUtils.are;

import org.junit.Test;

import pipez.core.Block;
import pipez.core.SimpleBlock;

public class SortPipeTest {

	@Test
	public void test_FieldSort() {
		SimpleBlock sb = new SimpleBlock("abc", "cde", "efg", "ghi" );
		
		SortPipe sort = SortPipe.create();
		Block b = sort.transform(sb);

		assertThat(b.values().length, is(4));
		assertThat(b.values(), are("abc", "cde", "efg", "ghi" ));
	}

	@Test
	public void test_ValueSort() {
		SimpleBlock sb = new SimpleBlock("xyz", "cde", "efg", "ghi" );
		
		SortPipe sort = SortPipe.create("efg");
		Block b = sort.transform(sb);

		assertThat(b.values().length, is(4));
		assertThat(b.values(), are("cde", "efg", "ghi", "xyz" ));
	}

	@Test
	public void test_ValueSortIgnoreCase() {
		SimpleBlock sb = new SimpleBlock("xyz", "cde", "efg", "ghi" );
		
		SortPipe sort = SortPipe.createIgnoreCase("EFg");
		Block b = sort.transform(sb);

		assertThat(b.values().length, is(4));
		assertThat(b.values(), are("cde", "efg", "ghi", "xyz" ));
	}
}
